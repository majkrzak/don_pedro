from ..utils.translate import translate
from ..utils.fetch import fetch
from ..utils.italize import italize
from datetime import datetime, timezone

URL = 'https://www.ravintolailves.fi/'
PATCH = '//h3[contains(text(),"{:%-d.%-m}")]/following-sibling::p[1]/text()'


def ilves():
	menu_fi = fetch(URL, PATCH.format(datetime.now(timezone.utc)))
	menu_en = translate(menu_fi)
	return {
		'title': 'Ilves lounas menu',
		'title_link': URL,
		'text': '{}\n\n{}'.format(menu_fi, italize(menu_en))
	}
