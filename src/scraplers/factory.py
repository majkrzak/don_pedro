from ..utils.translate import translate
from ..utils.fetch import fetch
from ..utils.italize import italize
from datetime import datetime, timezone

URL = 'http://www.factorykamppi.com/lounas/'
PATCH = '//div[@id="content"]//h3[contains(text(),"{:%-d.%-m}")]/following-sibling::p[1]/text()'


def factory():
	menu_fi = fetch(URL, PATCH.format(datetime.now(timezone.utc)))
	menu_en = translate(menu_fi)
	return {
		'title': 'Factory lounas menu',
		'title_link': URL,
		'text': '{}\n\n{}'.format(menu_fi, italize(menu_en))
	}
