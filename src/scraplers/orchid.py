from ..utils.fetch import fetch
from ..utils.fetch_pdf_as_image import fetch_pdf_as_image
from urllib.parse import urlparse
from datetime import datetime, timezone
from pyocr.libtesseract import image_to_string
from pyocr.builders import TextBuilder

URL = 'https://asiakas.kotisivukone.com/files/thaimaalainenravintola.palvelee.fi/Kamppi_lounas_{:%Y}_week_{:%V-%Y}.pdf'
PATH = '//a[contains(text(),"LOUNASLISTA {}")]/@href'
DAYS = ['MA', 'TI', 'KE', 'TO', 'PE']


def orchid():
	now = datetime.now(timezone.utc)
	menu_url = URL.format(now, now)
	pdf = fetch_pdf_as_image(menu_url)

	width, height = pdf.size
	pdf.crop(
		left=int(.11 * width),
		top=int(height * .265),
		right=int(.89 * width),
		bottom=int(height * .74),
	)

	try:
		txt = image_to_string(
			pdf,
			lang='eng+fin+tha',
			builder=TextBuilder()
		)
	except:
		txt = ':cry:'

	return {
		'title': 'Thai orchid lounas menu',
		'title_link': menu_url,
		'text': txt
	}
