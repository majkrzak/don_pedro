from slackclient import SlackClient
from os import environ

API_TOKEN = environ["SLACK_API_TOKEN"]
CHANNEL = environ["SLACK_CHANNEL"]

from .scraplers.factory import factory
from .scraplers.ilves import ilves
from .scraplers.orchid import orchid

from .utils.schedule import schedule

schedule(8, 12, lambda: SlackClient(API_TOKEN).api_call(
	'chat.postMessage',
	**{
		'channel': CHANNEL,
		'attachments': [
			factory(),
			ilves(),
			orchid()
		]
	}
))
