import setuptools

NAME = 'don_pedro'
REPOSITORY = 'https://majkrzak@bitbucket.org/majkrzak/don_pedro.git'
CORE_DEPENDENCIES = [
	'lxml',
	'py_translator',
	'slackclient',
	'pyocr',
	'wand'
]
BUILD_DEPENDECIES = [
	'docker',
	'wheel'
]
HOST_DEPENDENCIES = [
	'python3',
	'py3-lxml',
	'py3-pillow',
	'tesseract-ocr-dev',
	'tesseract-ocr-data-fin',
	'tesseract-ocr-data-tha',
	'imagemagick6',
	'imagemagick6-dev'
]
DOCKERFILE = f'''
	FROM alpine as build
	RUN apk add python3
	COPY . .
	RUN python3 setup.py bdist_wheel --dist-dir .
	
	FROM alpine
	RUN apk add {" ".join(HOST_DEPENDENCIES)}
	COPY --from=build {NAME}-0.0.0-py3-none-any.whl .
	RUN python3 -m pip install {NAME}-0.0.0-py3-none-any.whl
	ENTRYPOINT ["python3", "-m", "{NAME}"]
	CMD []
'''
DOCKERFILE_CI = f'''
    FROM alpine
    RUN apk add git python3
    WORKDIR {NAME}
    RUN git init && git remote add origin {REPOSITORY}
    CMD while :; \
        do \
            git fetch origin master; \
            if \
                ! git rev-parse HEAD \
                || [ "$(git rev-parse HEAD)" != "$(git rev-parse FETCH_HEAD)" ]; \
            then \
                git checkout FETCH_HEAD; \
                python3 setup.py dockerize deploy; \
            else \
                sleep 300; \
            fi; \
        done;
'''


def dockerizer(tag, dockerfile):
	class command(setuptools.Command):
		user_options = []

		def initialize_options(self):
			pass

		def finalize_options(self):
			pass

		def run(self):
			from docker import from_env
			from docker.errors import BuildError
			import docker.api.build
			docker.api.build.process_dockerfile = lambda dockerfile, path: ('Dockerfile', dockerfile)

			try:
				img, log = from_env().images.build(
					path='.',
					rm=True,
					tag=tag,
					dockerfile=dockerfile
				)
				for line in log:
					print(line)
			except BuildError as e:
				for line in e.build_log:
					print(line)

	return command


def deployer(name, environment=None, volumes=None):
	class command(setuptools.Command):
		user_options = []

		def initialize_options(self):
			pass

		def finalize_options(self):
			pass

		def run(self):
			from os import environ
			from docker import from_env
			from docker.errors import NotFound

			try:
				from_env().containers.get(name).remove(force=True)
			except NotFound:
				pass

			from_env().containers.run(
				image=name,
				name=name,
				volumes=volumes,
				environment={
					key: environ[key]
					for key in environment
				} if environment else None,
				detach=True,
				restart_policy={'Name': 'always'}
			)

	return command


setuptools.setup(
	name=NAME,
	description='Best food spy ever',
	author='Piotr Majrzak',
	author_email='petrol.91@gmail.com',
	license='Permission to use, copy, modify, and distribute this software for any '
	        + 'purpose with or without fee is hereby granted, provided that the above '
	        + 'copyright notice and this permission notice appear in all copies.',
	packages=[
		NAME,
		NAME + '.utils',
		NAME + '.scraplers'
	],
	package_dir={NAME: './src'},
	install_requires=CORE_DEPENDENCIES,
	setup_requires=BUILD_DEPENDECIES,
	cmdclass={
		'dockerize': dockerizer(NAME, DOCKERFILE),
		'dockerize_ci': dockerizer(f'{NAME}_ci', DOCKERFILE_CI),
		'deploy': deployer(
			NAME,
			['SLACK_API_TOKEN', 'SLACK_CHANNEL']
		),
		'deploy_ci': deployer(
			f'{NAME}_ci',
			['SLACK_API_TOKEN', 'SLACK_CHANNEL'],
			['/var/run/docker.sock:/var/run/docker.sock'],
		),
	}
)
